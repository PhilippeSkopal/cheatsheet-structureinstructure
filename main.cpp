#include <iostream>

// Prototype pour afficher les noms des tableaux
void DisplayTabOne(struct Parent *Table, int SizeTable);
void DisplayTabTwo(struct Child *Table, int SizeTable);

// Struct Parent, qui contient les parents de malcolm
struct Parent{
    std::string Name;                                        // Pour le nom du personnage
    int Age;
    float Size;
    std::string Work;
};

// Struct Child, qui contient Malcolm et ses fr�res
struct Child{
    std::string Name;                                        // Pour le nom du personnage
    int Age;
    float Size;
    std::string School;
};

struct Family{
    /* struct */ Parent Parents[2];                           // Tableau de la structure parents,
    /* struct */ Child Children[5];                           // le struct devant ne doit pas �tre mis
};

int main()
{
    /* struct */Family MalcolmFamily;                      // le struct devant ene doit pas �tre mis

    MalcolmFamily.Parents[0].Name = "Lois";
    MalcolmFamily.Parents[1].Name = "Hal";

    MalcolmFamily.Children[0].Name = "Malcolm";
    MalcolmFamily.Children[1].Name = "Francis";
    MalcolmFamily.Children[2].Name = "Reese";
    MalcolmFamily.Children[3].Name = "Dewey";
    MalcolmFamily.Children[4].Name = "Jamie";

    std::cout<< "liste des parents de la famille de Malcolm" << std::endl << std::endl;
    DisplayTabOne(MalcolmFamily.Parents, 2);

    std::cout << std::endl << "liste des enfants de la famille de Malcolm " << std::endl << std::endl;
    DisplayTabTwo(MalcolmFamily.Children, 5);

    return 0;
}

void DisplayTabOne(struct Parent *Table, int SizeTable)
{
    int i = 0;
    for(i=0; i<SizeTable; i++){
        std::cout << Table[i].Name << std::endl;
    }
}
void DisplayTabTwo(struct Child *Table, int SizeTable)
{
    int i = 0;
    for(i=0; i<SizeTable; i++){
        std::cout << Table[i].Name << std::endl;
    }
}
